## Tugas 1 PPW
Repo ini untuk tugas 1 kelompok PPW

## Anggota Kelompok
- Athallah Annafis - 1706075022 - Halamman list program
- Annida Safira A. - 1706040050 - Form Registrasi Donatur
- I Gusti Putu Agastya Indrayana - 1706074940 - Form Submit donasi
- M. Farras Hakim - 1706024513 - Halaman news/berita

## Link Heroku
http://ppwbisa.herokuapp.com/

## Status pipeline
[![pipeline status](https://gitlab.com/FarrasHakim/a5/badges/master/pipeline.svg)](https://gitlab.com/FarrasHakim/a5/commits/master)

## Status code coverage
[![coverage report](https://gitlab.com/FarrasHakim/a5/badges/master/coverage.svg)](https://gitlab.com/FarrasHakim/a5/commits/master)
